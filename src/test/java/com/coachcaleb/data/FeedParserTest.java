package com.coachcaleb.data;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: nortons
 * Date: 1/30/13
 * Time: 9:26 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(RobolectricTestRunner.class)
public class FeedParserTest {

    @Test
    @Ignore
    public void testParse() throws Exception {
        FeedParser parser = new FeedParser();

        try {
//            InputStream in = new FileInputStream(testFile);
            InputStream in = FeedParser.class.getClassLoader().getResourceAsStream("atom.xml");
            System.out.println(in.toString());
            System.out.println(in.available());
            parser.parse(in);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            fail(e.getLocalizedMessage());
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}
