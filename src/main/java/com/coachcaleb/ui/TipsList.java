package com.coachcaleb.ui;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.coachcaleb.R;
import com.coachcaleb.data.FeedParser;
import com.coachcaleb.data.Tip;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

/**
 * Created with IntelliJ IDEA.
 */
public class TipsList extends SherlockListActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tips_list);

        // Create a progress bar to display while the list loads
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        progressBar.setIndeterminate(true);
        getListView().setEmptyView(progressBar);

        // Must add the progress bar to the root of the layout
        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        root.addView(progressBar);
        loadTips();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        CharSequence text = "You clicked on id " + id;

        Toast toast = Toast.makeText(this, text, 5);
        toast.show();

    }

    public void sendLoadTips(View view){
        loadTips();
    }

    public void loadTips() {

        new FetchTipsTask().execute(""); //TODO: finalize parameters

    }

    private class FetchTipsTask extends AsyncTask<String, Integer, List> {
        @Override
        protected List doInBackground(String... urls) {

            FeedParser parser = new FeedParser();
            List<Tip> tips = null;
            try {
                tips = parser.parse(getApplicationContext(), urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }


            return tips;
        }

        @Override
        protected void onPostExecute(List list) {
            Context context = getApplicationContext();
            int duration = 5;
            CharSequence text = "fetching tips " + (list != null ? list.size() : " null list");

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            TipsAdapter tipsAdapter = new TipsAdapter(context, list);
            setListAdapter(tipsAdapter);

        }

    }
}