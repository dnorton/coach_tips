package com.coachcaleb.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.coachcaleb.R;
import com.coachcaleb.data.Tip;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 */
public class TipsAdapter extends BaseAdapter {

    private List<Tip> tips;
    private final Context context;
    private static final String LOGNAME = TipsAdapter.class.getName();

    public TipsAdapter(Context context, List<Tip> tips) {
        this.context = context;
        this.tips = tips;
    }

    @Override
    public int getCount() {
        return this.tips == null ? 0 : this.tips.size();
    }

    @Override
    public Object getItem(int position) {
        return this.tips.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(LOGNAME, "calling getView with position =" + Integer.toString(position));
        Tip tip = this.tips.get(position);

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = View.inflate(this.context, R.layout.tip_view, null);
        TextView tipTitle = (TextView) rowView.findViewById(R.id.title);
        tipTitle.setText(tip.getTitle());


        return rowView;
    }
}
