package com.coachcaleb.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Class used to create the SQLite database to store Tips from
 * coachcaleb.com
 *
 * Created with IntelliJ IDEA.
 */
public class TipsDatabase extends SQLiteOpenHelper {

    private static final String LOG_NAME = "TipsDatabase";

    private static final String DB_NAME =  "tip_data";
    private static final int DB_VERSION = 1;

    public static final String TIPS_TABLE = "tips";

    public static final String ID = "_id";
    public static final String COL_TITLE = "title";
    public static final String COL_LINK = "link";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_PUBDATE = "pub_date";
    public static final String COL_CREATOR = "creator";
    public static final String COL_GUID = "guid";
    
    private static final String TIPS_TABLE_DDL = TipsDatabase.generateTipsTableDdl();

    private static String generateTipsTableDdl() {
        String textNotNull = " text not null,";

        StringBuilder builder = new StringBuilder("create table ");
        builder.append(TIPS_TABLE).append(" (").append(ID).append(" integer primary key autoincrement, ");
        builder.append(COL_TITLE).append(textNotNull);
        builder.append(COL_LINK).append(textNotNull);
        builder.append(COL_DESCRIPTION).append(textNotNull);
        builder.append(COL_CREATOR).append(textNotNull);
        builder.append(COL_GUID).append(" INTEGER,");
        builder.append(COL_PUBDATE).append(" INTEGER);");

        return builder.toString();
    }

    public TipsDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TIPS_TABLE_DDL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_NAME, "Upgrading database. Existing content will be lost. [" + oldVersion + "]->[" + newVersion + "]");
        db.execSQL("DROP TABLE IF EXISTS " + TIPS_TABLE);
        onCreate(db);
    }
}
