package com.coachcaleb.data;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import com.coachcaleb.R;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Parse the ATOM feed from coachcaleb.com
 * code modified from http://developer.android.com/training/basics/network-ops/xml.html
 *
 */
public class FeedParser {

    protected static final String ns = null;

    protected static final String LOG_TAG = FeedParser.class.getName();

    public List<Tip> parse(Context context, String url) throws IOException, XmlPullParserException {
        InputStream in = context.getAssets().open("atom.xml");
//        InputStream in = FeedParser.class.getClassLoader().getResourceAsStream("atom.xml");
        Log.w(LOG_TAG, "InputStream available: " + in.available());
        return parse(in);
    }

    public List<Tip> parse(InputStream in) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        List<Tip> tips = null;
        try {
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            tips = readFeed(parser);
        } catch (Exception ex) {
            Log.e(LOG_TAG, ex.getMessage());
        } finally {
            if (in != null) in.close();
        }
        return tips;
    }

    protected List<Tip> readFeed(XmlPullParser parser) throws IOException, XmlPullParserException {

        List<Tip> tips = new LinkedList<Tip>();

        parser.require(XmlPullParser.START_TAG, ns, "rss");

        while ( parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG ||
                    (parser.getName() != null  && parser.getName().equals("channel")))
                continue;


            String name = parser.getName();

            if (name.equals("item")) {
                tips.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }

        return tips;
    }

    private Tip readEntry(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "item");

        Tip tip = new Tip();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG)
                  continue;

            String name = parser.getName();

            if (name.equals("title")) {
                tip.setTitle(readTextNode(parser, "title"));
            } else if (name.equals("link")) {
                tip.setLink(readTextNode(parser, "link"));
            } else if (name.equals("description")) {
                tip.setDescription(readTextNode(parser, "description"));
            } else {
                skip(parser);
            }
        }


        return tip;
    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }

    private String readTextNode(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String text = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return text;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
