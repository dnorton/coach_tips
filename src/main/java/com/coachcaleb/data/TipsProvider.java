package com.coachcaleb.data;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * modified from http://mobile.tutsplus.com/tutorials/android/android-sdk_content-providers/
 */
public class TipsProvider extends ContentProvider {

    private static final String AUTHORITY = "com.coachcaleb.data.TipsProvider";
    public static final int TIPS = 100;
    public static final int TIP_ID = 110;
    private static final String TIPS_BASE_PATH = "tips";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TIPS_BASE_PATH);
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/mt-tips";
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/mt-tips";

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, TIPS_BASE_PATH, TIPS);
        sURIMatcher.addURI(AUTHORITY, TIPS_BASE_PATH + "/#", TIP_ID);
    }

    private static final String DEBUG_TAG = "TipsProvider";

    private TipsDatabase mDB;
    @Override
    public boolean onCreate() {
        mDB = new TipsDatabase(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TipsDatabase.TIPS_TABLE);
        int uriType = sURIMatcher.match(uri);
        switch (uriType){
            case TIP_ID:
                queryBuilder.appendWhere(TipsDatabase.ID + "="
                + uri.getLastPathSegment());
                break;
//            case TIPS:
                //no filter
//                break;
            default:
                throw new IllegalArgumentException("Unknown URL");
        }
        Cursor cursor = queryBuilder.query(mDB.getReadableDatabase(), projection, selection,
                selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case TIPS:
                return CONTENT_TYPE;
            case TIP_ID:
                return CONTENT_ITEM_TYPE;
            default:
                return null;
        }

    }

    /**
     * https://code.google.com/p/android-mtlist-tutorial/source/browse/trunk/src/com/mamlambo/tutorial/tutlist/data/TutListProvider.java
     * @param uri
     * @param values
     * @return
     */
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        if (uriType != TIPS) {
            throw new IllegalArgumentException("Invalid URI for insert");
        }
        SQLiteDatabase sqlDB = mDB.getWritableDatabase();
        try {
            long newID = sqlDB.insertOrThrow(TipsDatabase.TIPS_TABLE,
                    null, values);
            if (newID > 0) {
                Uri newUri = ContentUris.withAppendedId(uri, newID);
                getContext().getContentResolver().notifyChange(uri, null);
                return newUri;
            } else {
                throw new SQLException("Failed to insert row into " + uri);
            }
        } catch (SQLiteConstraintException e) {
            Log.i(DEBUG_TAG, "Ignoring constraint failure.");
        }
        return null;
    }
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mDB.getWritableDatabase();

        int rowsAffected;

        switch (uriType) {
            case TIP_ID:
                String id = uri.getLastPathSegment();
                StringBuilder modSelection = new StringBuilder(TipsDatabase.ID
                        + "=" + id);

                if (!TextUtils.isEmpty(selection)) {
                    modSelection.append(" AND " + selection);
                }

                rowsAffected = sqlDB.update(TipsDatabase.TIPS_TABLE,
                        values, modSelection.toString(), null);
                break;
            case TIPS:
                rowsAffected = sqlDB.update(TipsDatabase.TIPS_TABLE,
                        values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown or Invalid URI");
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }
}
